#include "NeuralNetwork.h"

NeuralNetwork::NeuralNetwork()
{
	inputLayerSize = 2;
	hiddenLayerSize = 3;
	outputLayerSize = 1;
	this->initNetwork();
}


NeuralNetwork::NeuralNetwork(int inLayerSize, int hidLayerSize, int outLayerSize)
{
	inputLayerSize = inLayerSize;
	hiddenLayerSize = hidLayerSize;
	outputLayerSize = outLayerSize;
	this->initNetwork();
}

NeuralNetwork::~NeuralNetwork()
{
	delete w1;
	delete w2;
}

void NeuralNetwork::initNetwork()
{
	w1 = new MatrixXd(inputLayerSize, hiddenLayerSize); //weights for input layer to hidden layer
	w2 = new MatrixXd(hiddenLayerSize, outputLayerSize); //weights for hidden layer to output layer
	srand(time(NULL)); //changes the seed for random
	w1->setRandom(); //randomizes coefficients
	w2->setRandom(); //randomizes coefficients

	#ifdef VERBOSE
		cout << "random w1: \n" << *w1 << endl << endl;
		cout << "random w2: \n" << *w2 << endl << endl;
	#endif
}

void NeuralNetwork::forward(MatrixXd inputData)
{
	if (inputData.cols() != inputLayerSize) 
	{
		cout << "Error: number of columns does not match number of inputs of the NN" << endl;
		return;
	}

	z2 = (inputData)*(*w1);
	a2 = z2.unaryExpr(&sigmoid);
	z3 = a2*(*w2);
	yhat = z3.unaryExpr(&sigmoid);

	#ifdef VERBOSE
		cout << "z2: \n" << z2 << endl << endl;
		cout << "a2: \n" << a2 << endl << endl;
		cout << "z3: \n" << z3 << endl << endl;
		cout << "yhat: \n" << yhat << endl << endl;
	#endif
}

MatrixXd NeuralNetwork::getYhat()
{
	return yhat;
}

MatrixXd NeuralNetwork::getZ3()
{
	return z3;
}

MatrixXd NeuralNetwork::getA2()
{
	return a2;
}

MatrixXd NeuralNetwork::getW2()
{
	return *w2;
}

MatrixXd NeuralNetwork::getZ2()
{
	return z2;
}

MatrixXd NeuralNetwork::getW1()
{
	return *w1;
}

void NeuralNetwork::setW1(MatrixXd w1New)
{
	*w1 = w1New;
}

void NeuralNetwork::setW2(MatrixXd w2New)
{
	*w2 = w2New;
}


