#include "main.h"


int main()
{
	double cost;
	NeuralNetwork myNN(2,30,1);
	NNTrainer trainer;
	MatrixXd inData(3, 2); //input data must be normalized!
	inData(0, 0) = 0.5;
	inData(0, 1) = 0.1;

	inData(1, 0) = 0.1;
	inData(1, 1) = 0.5;

	inData(2, 0) = 0.96;
	inData(2, 1) = 0.9;


	cout << "inData:\n" << inData<< endl << endl;

	myNN.forward(inData);
	MatrixXd outData(3,1); //output data
	outData(0, 0) = 0.1;
	outData(1, 0) = 0.6;
	outData(2, 0) = 0.95;

	cout << "Real Output:\n" <<outData << endl << endl;
	int i = 0;
	do
	{
		cost = trainer.costFunction(myNN, inData, outData); //executes neural network and calculates cost function (only call this if u need to display cost to user since its not required and redundant)
		//cout << "Neural Network outputs:\n" << myNN.getYhat() << endl << endl;
		//cout << "Cost #" << i+1 << ": \n" << cost << endl << endl;
		trainer.costFunctionPrime(myNN, inData, outData); //executes neural network and caluclates cost function prime which is used in gradient descent
		trainer.gradientDescent(myNN); //optimize weights
		i++;
	} while (cost > 0.000000001);
	cout << "Neural Network outputs:\n" << myNN.getYhat() << endl << endl;
	cout << "Cost #" << i+1 << ": \n" << cost << endl << endl;
	cout << "Neural Network weight1:\n" << myNN.getW1() << endl << endl;
	cout << "Neural Network weight2:\n" << myNN.getW2() << endl << endl;



	char x;
	while (1)
	{
		cin >> x;
		cout << x;
	}

	return 0;
}
