
#ifndef NEURALNETWORK_H_
#define NEURALNETWORK_H_
#include <iostream>
using namespace std;
#include "Eigen/Dense"
using namespace Eigen;
#include <time.h>
#include "NN_Math.h"

//#define VERBOSE


//https://www.youtube.com/watch?v=UJwK6jAStmg


class NeuralNetwork
{
	public:
		NeuralNetwork();
		NeuralNetwork(int, int, int); //overloaded constructor for user specified network size
		~NeuralNetwork(); //destructor

		//Public member functions
		void forward(MatrixXd inputData); //performs the neural network operation on provided input data
		MatrixXd getYhat();
		MatrixXd getZ3();
		MatrixXd getA2();
		MatrixXd getW2();
		MatrixXd getZ2();
		MatrixXd getW1();
		void setW1(MatrixXd newW1);
		void setW2(MatrixXd newW2);

	private:
		int inputLayerSize; //number of input neurons
		int outputLayerSize; //number of output neurons
		int hiddenLayerSize; //number of hidden neurons (only one hidden middle layer for now)

		MatrixXd* w1; //weights for input layer to hidden layer
		MatrixXd* w2; //weights for hidden layer to output layer

		MatrixXd z2; //intermediate activity matrix
		MatrixXd a2; //activity post sigmoid
		MatrixXd z3; //intermediate activity matrix
		MatrixXd yhat; //intermediate activity matrix

		//Private member function prototypes
		void initNetwork();

};


#endif /* NEURALNETWORK_H_ */
