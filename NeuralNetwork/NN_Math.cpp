#include "NN_Math.h"

double sigmoid(double alpha)
{
	return 1.0 / (1.0 + exp(-1.0*alpha));
}

double sigmoidPrime(double alpha)
{
	return exp(-1.0*alpha) / ((1 + exp(-1.0*alpha))*(1 + exp(-1.0*alpha)));
}
