#include "NNTrainer.h"

NNTrainer::NNTrainer()
{
	descentSpeed = 1;
}

NNTrainer::~NNTrainer()
{

}

//computes cost function
double NNTrainer::costFunction(NeuralNetwork &NN, MatrixXd inputData, MatrixXd desiredOutputData)
{
	//sum of 0.5*error^2
	NN.forward(inputData); //compute estimated output using inputData set and Neural Network

	MatrixXd J = ((desiredOutputData - NN.getYhat()).array()*(desiredOutputData - NN.getYhat()).array()*0.5);
	double sum = J.sum();

	return sum;
}
//compute how cost function changes with respect to neural network weights
void NNTrainer::costFunctionPrime(NeuralNetwork &NN, MatrixXd inputData, MatrixXd desiredOutputData)
{
	MatrixXd delta3; //back propagating error
	//MatrixXd dJdW2; //how cost function changes with respect to w2 (weights from hidden to output layer)

	MatrixXd delta2; //back propagating error
	//MatrixXd dJdW1; //how cost function changes with respect to w1 (weights from input to hidden layer)

	NN.forward(inputData); //compute estimated output using inputData set and Neural Network

	//computes derrivative with respect to w1 and w2
	MatrixXd errorTemp = desiredOutputData - NN.getYhat();
	errorTemp = errorTemp *-1.0;
	MatrixXd sigprimeTemp = NN.getZ3().unaryExpr(&sigmoidPrime);
	delta3 = errorTemp.array()*sigprimeTemp.array(); //compute back propagating error by ELEMENT WISE MULTIPLICATION NOT MATRIX MULT (hence why we need to convert to array)
	dJdW2 = (NN.getA2().transpose()) * delta3;

	errorTemp = (NN.getW2().transpose());
	errorTemp = delta3 * errorTemp;
	sigprimeTemp = (NN.getZ2()).unaryExpr(&sigmoidPrime);
	delta2 = errorTemp.array() * sigprimeTemp.array();
	dJdW1 = (inputData.transpose())*delta2;
}

//performs the gradient descent using computed derrivatives of cost function using cost function prime method
void NNTrainer::gradientDescent(NeuralNetwork& NN)
{
	NN.setW1(NN.getW1() - descentSpeed*dJdW1);	
	NN.setW2(NN.getW2() - descentSpeed*dJdW2);
}
