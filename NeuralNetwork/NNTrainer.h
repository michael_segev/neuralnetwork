
#ifndef TRAINER_H_
#define TRAINER_H_

#include <iostream>
using namespace std;
#include "Eigen/Dense"
using namespace Eigen;
#include <time.h>
#include "NN_Math.h"
#include "NeuralNetwork.h"

//#define VERBOSE


//https://www.youtube.com/watch?v=UJwK6jAStmg


class NNTrainer
{
public:
	NNTrainer();
	~NNTrainer(); //destructor
	//Public member functions

	double costFunction(NeuralNetwork& NN, MatrixXd inputData, MatrixXd desiredOutputData); //performs the cost function using current weights on neural network
	void costFunctionPrime(NeuralNetwork& NN, MatrixXd inputData, MatrixXd desiredOutputData); //computes derrivative with respect to w1 and w2 using user provided real output data to given input data
	void gradientDescent(NeuralNetwork& NN);
private:
	MatrixXd dJdW2; //how cost function changes with respect to w2 (weights from hidden to output layer)
	MatrixXd dJdW1; //how cost function changes with respect to w1 (weights from input to hidden layer)
	double descentSpeed;
	

};


#endif
