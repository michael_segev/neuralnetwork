#ifndef __NNMATH_H__
#define __NNMATH_H__
#include <iostream>
using namespace std;
#include "Eigen/Dense"
using namespace Eigen;

double sigmoid(double alpha);
double sigmoidPrime(double alpha);
#endif
