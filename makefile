DIR_OUTPUT = ./Output/
DIR_SOURCE = ./NeuralNetwork/

#make list of source files
SRC_FILES += $(DIR_SOURCE)main.cpp
SRC_FILES += $(DIR_SOURCE)NeuralNetwork.cpp
SRC_FILES += $(DIR_SOURCE)NN_Math.cpp
SRC_FILES += $(DIR_SOURCE)NNTrainer.cpp

#make list of required object files
OBJ = $(addprefix $(DIR_OUTPUT), $(addsuffix .o, $(basename $(notdir $(SRC_FILES)))))

build: $(OBJ)
	@echo Linking...
	g++ -o $(DIR_OUTPUT)NeuralNetwork.exe $(OBJ)
	@echo Done!

#if *.o not found, look for *.cpp and compile it!
$(DIR_OUTPUT)%.o: $(DIR_SOURCE)%.cpp | mkdirOutput
	@echo Compiling $(notdir $<) into $(notdir $@) ...
	@g++ -o $@ -c $< -I $(DIR_SOURCE)

#make Output directory if it doesn't exist!
mkdirOutput:
	@mkdir -p $(DIR_OUTPUT)

clean:
	@rm -fr $(DIR_OUTPUT)
